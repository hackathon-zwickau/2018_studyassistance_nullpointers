package utility;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import application.model.ordnerstruktur.Kurs;


public class Ordnerbuilder
{
	private String path;
	
	public Ordnerbuilder()
	{
		
	}
	
	public Optional<List<Kurs>> createKurs()
	{
		Optional<List<Kurs>> okurs = Optional.ofNullable(null);
		List<Kurs> kurs = new ArrayList<>();
		int kursID = 0;
		Optional<String> text = openPDF();
		if (text.isPresent())
		{
			StringTokenizer st = new StringTokenizer(text.get());
			while (st.hasMoreTokens())
			{
				String token = st.nextToken();
				//int comparefac = ;
				if (token.contains("PTI"))
				{
					String Kursname = token + "";
					for (int i=0; i<3 ; i++) {
						Kursname = Kursname + " " + st.nextToken();
					}
					kurs.add(new Kurs(kursID, Kursname)); 
					kursID++;
				}
			}
			okurs = Optional.ofNullable(kurs);
		}
		return okurs;
	}
	
	
	public Optional<String> openPDF ()
	{
	    PdfCrawler pdfManager = new PdfCrawler();
	    pdfManager.setFilePath(path);
	    try {
	    	Optional<String> text = Optional.ofNullable(null);
	        text = Optional.ofNullable(pdfManager.toText());
	        return text;
	    } catch (IOException ex) {
	        Logger.getLogger(Ordnerbuilder.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    return null;
	}
	
	public void setPath(String path)
	{
		this.path = path;
	}
}
