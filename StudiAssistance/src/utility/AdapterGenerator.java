package utility;

import java.util.ArrayList;
import java.util.List;

import application.Main;
import application.model.ordnerstruktur.Kurs;
import application.model.ordnerstruktur.KursAdapter;
import application.model.ordnerstruktur.Ordner;
import application.model.ordnerstruktur.OrdnerAdapter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AdapterGenerator {

	private Main main;
	private ObservableList<KursAdapter> kursAdapter = FXCollections.observableArrayList();
	private ObservableList<OrdnerAdapter> ordnerAdapter = FXCollections.observableArrayList();
	
	public AdapterGenerator()
	{
		
	}
	
	public ObservableList<KursAdapter> kursAdapterGenerator(List<Kurs> kurs)
	{
		List<Kurs> kurse = kurs;
		for (Kurs k:kurse)
		{
			kursAdapter.add(new KursAdapter(k));
		}
		return kursAdapter;
	}
	
	public ObservableList<OrdnerAdapter> ordnerAdapterGenerator(List<Kurs> kurs)
	{
		for (Kurs k:kurs)
		{
			ordnerAdapter.add(new OrdnerAdapter(new Ordner(k.getKursname(), "")));
		}
		return ordnerAdapter;
	}
	
}
