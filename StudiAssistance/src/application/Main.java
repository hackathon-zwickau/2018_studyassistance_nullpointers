package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import application.model.ordnerstruktur.Kurs;
import application.model.programmInstalation.NeededPrograms;
import application.model.programmInstalation.NeededProgramsAdapter;
import application.view.DownloadProgramsController;

import application.model.ordnerstruktur.Ordner;

import application.view.StudiAssistanceMainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import utility.Ordnerbuilder;

public class Main extends Application
{
	private List<Kurs> kurs;

	private Stage primaryStage;
	private AnchorPane rootLayout;

	private NeededPrograms neededPrograms = new NeededPrograms();
	public NeededProgramsAdapter programs = new NeededProgramsAdapter(neededPrograms);

	@Override
	public void start (Stage primaryStage)
	{
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Studi Assistance");

		initRootLayout();
	}

	public Main ()
	{

		kurs = new ArrayList<>();

	}

	public void initializeKurs (String path)
	{
		kurs.clear();
		Ordnerbuilder o = new Ordnerbuilder();
		o.setPath(path);
		Optional<List<Kurs>> okurs = o.createKurs();
		if (okurs.isPresent())
		{
			kurs.addAll(okurs.get());
		}
	}

	public void initRootLayout ()
	{
		try
		{
			// load Scene and set
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/StudiAssistanceMain.fxml"));
			rootLayout = (AnchorPane) loader.load();

			// Show scene with root layout
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();

			// Initialize controller
			StudiAssistanceMainController controller = loader.getController();
			controller.setMain(this);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public Optional<String> fileChoser ()
	{
		Optional<String> path = Optional.ofNullable(null);
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File file = fileChooser.showOpenDialog(primaryStage);
		if (file != null)
		 {
			 path = Optional.ofNullable(file.getAbsolutePath());
		 }
		return path;
	}
	
	public Optional<String> directoryChooser()
	{
		Optional<String> path = Optional.ofNullable(null);
		final DirectoryChooser directoryChooser = new DirectoryChooser();
		 directoryChooser.setTitle("Select Some Directories");
		 File dir = directoryChooser.showDialog(primaryStage);
		 if (dir != null)
		 {
			 path = Optional.ofNullable(dir.getAbsolutePath());
		 }
		 return path;
	}

	public List<Kurs> getKurs ()
	{
		return kurs;
	}

	public Stage getStage()
	{
		return primaryStage;
	}
	
	public void setInstallView ()
	{

		try
		{
			FXMLLoader loader = new FXMLLoader();

			loader.setLocation(Main.class.getResource("view/InstallOverlay.fxml"));
			AnchorPane downloadPrograms = (AnchorPane) loader.load();

			// set scene
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Download programs");
			dialogStage.initOwner(primaryStage);

			Scene scene = new Scene(downloadPrograms);
			dialogStage.setScene(scene);

			DownloadProgramsController controller = loader.getController();
			controller.setMain(this);
			controller.setNeededProgramsAdapter(programs);
			controller.setNeededPrograms(getNeededPrograms());
			controller.setStage(dialogStage);
			controller.initialize();
			controller.setCheckers();
			// controller.setCurrentHandy(sba);
			// controller.showSendedRecivedMess();

			dialogStage.showAndWait();

		}
		catch (IOException e)
		{
			e.printStackTrace();

		}
	}


	public void initProgramsAdapter (int semester)
	{
		neededPrograms.setSemester(semester);
		programs.setSemester(Integer.valueOf(semester));
		programs.setProgramNames();
	}

	public NeededPrograms getNeededPrograms ()
	{
		return neededPrograms;
	}

}
