package application.model.programmInstalation;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

/**
 * Function of this class is to download a file from a specific website via the
 * URL
 * 
 * @author Leon Bohr {@link jub17hm9@fh-zwickau.de}
 *
 */
public class DownloadViaURL
{
	private String installPath;

	private String url;
	private int port;

	/**
	 * downloading a file via URL
	 * 
	 * @param url
	 * @param port
	 *            - connections
	 * @param installPath
	 *            - Where to install the files
	 */
	public DownloadViaURL (String url, String installPath)
	{
		this.url = url;
		this.installPath = installPath;
	}

	/**
	 * downloads a file via FTP
	 * 
	 * @param downloadPath
	 *            - where to download
	 * @param urlPath
	 *            - the URL form where the file is downloaded
	 */
	public void connectAndDownload (String downloadPath, String urlPath)
	{

		String fileName = findeSpecificFileName(urlPath);

		try
		{
			FileUtils.copyURLToFile(new URL(urlPath), new File(downloadPath + fileName));
			System.out.println("file successfuly downloaded to " + downloadPath);
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String findeSpecificFileName (String urlLink)
	{
		int completeLenght = urlLink.length() - 1;
		int firstBackslashAt = 0;

		for (int i = completeLenght; i > 0; i--)
		{
			if (urlLink.charAt(i) == '/')
			{
				firstBackslashAt = i;
				break;
			}
		}

		StringBuilder finalFileName = new StringBuilder(urlLink);
		String fileName = finalFileName.subSequence(firstBackslashAt, completeLenght + 1).toString();

		return fileName;
	}

	public final String getInstallPath ()
	{
		return installPath;
	}

	public final String getURL ()
	{
		return url;
	}

	public final int getPort ()
	{
		return port;
	}

	public final void setInstallPath (String installPath)
	{
		this.installPath = installPath;
	}

	public final void setServer (String server)
	{
		this.url = server;
	}

	public final void setPort (int port)
	{
		this.port = port;
	}

}
