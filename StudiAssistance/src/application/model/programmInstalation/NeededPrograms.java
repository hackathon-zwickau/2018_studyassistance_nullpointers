package application.model.programmInstalation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * In this class are all programs(links) that are needed for the CA student.
 * 
 * @author Leon {@link jub17hm9@fh-zwickau.de}
 *
 */
public class NeededPrograms
{

	// each name has a download link -> hashmap
	private HashMap<String, String> kNameVLink = new HashMap<String, String>();

	// each semester has a map with kNameVLink
	private HashMap<Integer, HashMap<String, String>> progProSem = new HashMap<Integer, HashMap<String, String>>();

	private ArrayList<String> progNames = new ArrayList<String>();
	private ArrayList<String> programLinks;

	private int semester;

	/**
	 * initialize all needed programs with download links
	 * 
	 * @param semester
	 *            - initialize only the programs for this semester
	 */
	public NeededPrograms ()
	{
		semester1();
		semester2();
		semester3();
		semester4();
		// semester6();
		// semester7();
	}

	public void semester1 ()
	{
		// new themp Object
		// programLinks = new ArrayList<String>();

		// GDP 1
		kNameVLink.put("GreenFoot", "http://www.greenfoot.org/download/files/Greenfoot-windows-352.msi");
		// programLinks.add("http://www.greenfoot.org/download/files/Greenfoot-windows-352.msi");//
		// GreenFoot
		kNameVLink.put("Eclipse",
				"https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2018-09/Ra/eclipse-inst-win64.exe");
		// programLinks.add(
		// "https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2018-09/Ra/eclipse-inst-win64.exe");//
		// Eclipse
		kNameVLink.put("Java JDK_11",
				"http://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_windows-x64_bin.exe");
		// programLinks.add(
		// "http://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_windows-x64_bin.exe");//
		// JDK_11

		// Mensch-Computer-interaktion
		kNameVLink.put("Balsamiq Mockups_3", "https://builds.balsamiq.com/mockups-desktop/Balsamiq_Mockups_3.5.16.exe");
		// programLinks.add("https://builds.balsamiq.com/mockups-desktop/Balsamiq_Mockups_3.5.16.exe");//
		// Balsamiq

		// Program_Names
		// progNames.add("GreenFoot");
		// progNames.add("Eclipse");
		// progNames.add("Java JDK_11");
		// progNames.add("Balsamiq Mockups_3");

		// HASHMAP
		progProSem.put(0, kNameVLink);

	}

	public void semester2 ()
	{
		// new themp Object
		// programLinks = new ArrayList<String>();

		// GDP 2
		kNameVLink.put("JavaFX/JDK",
				"http://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_windows-x64_bin.exe");
		// programLinks.add(
		// "http://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_windows-x64_bin.exe");//
		// JDK_with_JavaFX
		kNameVLink.put("SceneBuilder",
				"https://gluonhq.com/products/scene-builder/#download/products/scene-builder/thanks/?dl=/download/scene-builder-9-windows-x64/SceneBuilder-10.0.0.exe");
		// programLinks.add(
		// "https://gluonhq.com/products/scene-builder/#download/products/scene-builder/thanks/?dl=/download/scene-builder-9-windows-x64/SceneBuilder-10.0.0.exe");//
		// SceneBuilder
		kNameVLink.put("Git",
				"https://github.com/git-for-windows/git/releases/download/v2.19.1.windows.1/Git-2.19.1-64-bit.exe");
		// programLinks.add(
		// "https://github.com/git-for-windows/git/releases/download/v2.19.1.windows.1/Git-2.19.1-64-bit.exe");//
		// Git

		// Program_Names
		// progNames.add()

		// HASHMAP
		progProSem.put(1, kNameVLink);
	}

	public void semester3 ()
	{
		// new themp Object
		// programLinks = new ArrayList<String>();

		// Logisim
		kNameVLink.put("Logisim",
				"https://downloads.sourceforge.net/project/circuit/2.7.x/2.7.1/logisim-win-2.7.1.exe?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fcircuit%2Ffiles%2Flatest%2Fdownload&ts=1541798371");
		// programLinks.add(
		// "https://downloads.sourceforge.net/project/circuit/2.7.x/2.7.1/logisim-win-2.7.1.exe?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fcircuit%2Ffiles%2Flatest%2Fdownload&ts=1541798371");
		kNameVLink.put("MARS",
				"http://courses.missouristate.edu/KenVollmar/mars/download.htm/MARS_4_5_Aug2014/Mars4_5.jar");
		// programLinks.add("http://courses.missouristate.edu/KenVollmar/mars/download.htm/MARS_4_5_Aug2014/Mars4_5.jar");//
		// MARS

		// OoSE
		kNameVLink.put("PMD_Tool",
				"https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.9.0/pmd-bin-6.9.0.zip");
		// programLinks.add("https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.9.0/pmd-bin-6.9.0.zip");//
		// PMD_BUGFinder

		// EvA
		kNameVLink.put("Spring-Tool-Suit_4",
				"http://download.springsource.com/release/STS4/4.0.1.RELEASE/dist/e4.9/spring-tool-suite-4-4.0.1.RELEASE-e4.9.0-win32.win32.x86_64.zip");
		// programLinks.add(
		// "http://download.springsource.com/release/STS4/4.0.1.RELEASE/dist/e4.9/spring-tool-suite-4-4.0.1.RELEASE-e4.9.0-win32.win32.x86_64.zip");//
		// spring

		// Wissensch.Arb.
		kNameVLink.put("LaTeX",
				"https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/basic-miktex-2.9.6850-x64.exe");
		// programLinks.add(
		// "https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/basic-miktex-2.9.6850-x64.exe");
		// // Optional_LaTeX

		// HASHMAP
		progProSem.put(2, kNameVLink);

	}

	public void semester4 ()
	{
		// new themp Object
		// programLinks = new ArrayList<String>();

		kNameVLink.put("Android-Studio",
				"https://dl.google.com/dl/android/studio/install/3.2.1.0/android-studio-ide-181.5056338-windows.exe");
		// programLinks.add(
		// "https://dl.google.com/dl/android/studio/install/3.2.1.0/android-studio-ide-181.5056338-windows.exe");
		// // Android_studio

		// HASHMAP
		progProSem.put(3, kNameVLink);
	}

	public void semester6 ()
	{

	}

	public void semester7 ()
	{

	}

	private void initializeHashMap (int semester)
	{
		switch (semester)
		{
		case (1) :
		{
			semester1();
		}
			break;
		case (2) :
		{
			semester2();
		}
			break;
		case (3) :
		{
			semester3();
		}
			break;
		case (4) :
		{
			semester4();
		}
			break;
		case (6) :
		{
			semester6();
		}
			break;
		case (7) :
		{
			semester7();
		}
			break;
		}
	}

	/**
	 * with the informations in witch semester and the name of the program it'll
	 * return the download link
	 * 
	 * @param semester
	 *            - program is in this semester
	 * @param programName
	 *            - name of the program
	 * @return - the programs downloadLink
	 */
	public String getDownloadLink (int semester, String programName)
	{
		HashMap<Integer, HashMap<String, String>> progsInSem = getAllProgProSemAsHashMap();
		HashMap<String, String> progs = progsInSem.get(Integer.valueOf(semester));

		return progs.get(programName);
	}

	/**
	 * returns an hashmap with progs and there download links
	 * 
	 * @param semester
	 *            - specific semester
	 * @return - hashMap with all progams and there download link
	 */
	public HashMap<String, String> getProgramsInSpeSem (Integer semester)
	{

		return progProSem.get(semester);
	}

	// public ArrayList<String> getOnlyProgNamesinSpeSem (Integer semester)
	// {
	// ArrayList<String> returnArray = (ArrayList<String>)
	// getProgramsInSpeSem(semester).keySet();
	// return returnArray;
	// }

	/**
	 * gets an hashMap where for each semester an hashmap with all program names and
	 * there download link
	 * 
	 * @return - KEY is the Semester, Value = (KEY program name, VALUE download
	 *         link)
	 */
	public HashMap<Integer, HashMap<String, String>> getAllProgProSemAsHashMap ()
	{
		return progProSem;
	}

	public void notifyBindingAdapter (NeededProgramsAdapter bindingAdapter)
	{
		bindingAdapter.setProgramNames();
		setSemester(bindingAdapter.getSemester().get());
	}

	public void setSemester (int semester)
	{
		this.semester = semester;
		initializeHashMap(semester);
	}
}
