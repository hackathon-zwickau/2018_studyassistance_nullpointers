package application.model.programmInstalation;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NeededProgramsAdapter
{

	private ArrayList<String> programNames = new ArrayList<String>();
	private NeededPrograms programs;

	private StringProperty programName;
	private IntegerProperty semester;

	public NeededProgramsAdapter (NeededPrograms programs)
	{
		this.programs = programs;
	}

	public void setProgamNameProperty (String progName)
	{
		this.programName = new SimpleStringProperty(progName);
	}

	public void setProgramNames ()
	{
		// ArrayList<String> programsInModel =
		// programs.getProgramsInSpeSem(Integer.valueOf(semester.get())).keySet();
		if (!programNames.isEmpty())
		{
			programNames.clear();
		}
		if (!programs.getProgramsInSpeSem(Integer.valueOf(semester.get())).isEmpty())
		{
			System.out.println("Test");
		}
		for (String name : programs.getProgramsInSpeSem(Integer.valueOf(semester.get())).keySet())
		{
			programNames.add(name);
		}
	}

	public final IntegerProperty getSemester ()
	{
		return semester;
	}

	public ArrayList<String> getProgramNames ()
	{
		return programNames;
	}

	public final void setSemester (final Integer semester)
	{
		this.semester = new SimpleIntegerProperty(semester);
	}

}
