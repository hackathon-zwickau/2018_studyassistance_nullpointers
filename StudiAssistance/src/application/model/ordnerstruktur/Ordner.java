package application.model.ordnerstruktur;

public class Ordner
{

	private String name;
	private String path;
	
	private OrdnerAdapter ordnerAdapter;
	
	public Ordner(String name, String path)
	{
		this.name = name;
		this.path = path;
	}
	
	public void notifyOrdnerAdapter()
	{
		ordnerAdapter.setName(name);
		ordnerAdapter.setPath(path);
	}
	
	public void setName(String name)
	{
		if (!this.name.equals(name))
		{
			this.name = name;
			notifyOrdnerAdapter();
		}
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setPath(String path)
	{
		if (!this.path.equals(path))
		{
			this.path = path;
			notifyOrdnerAdapter();
		}
	}
	
	public String getPath()
	{
		return path;
	}
	
	public void setOrdnerAdapter(OrdnerAdapter ordnerAdapter)
	{
		this.ordnerAdapter = ordnerAdapter;
	}
}
