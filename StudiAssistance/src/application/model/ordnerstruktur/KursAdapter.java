package application.model.ordnerstruktur;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KursAdapter
{

	private Kurs kurs;
	private final IntegerProperty ID;
	private StringProperty kursname;
	
	public KursAdapter(Kurs kurs)
	{
		this.kurs = kurs;
		this.ID = new SimpleIntegerProperty(kurs.getID());
		this.kursname = new SimpleStringProperty(kurs.getKursname());
		this.kurs.setKursAdapter(this);
	}
	
	public void notifyKurs()
	{
		kurs.setKursname(kursname.get());
	}

	public String getKursname()
	{
		return kursname.get();
	}

	public void setKursname(String kursname)
	{
		if (!this.kursname.equals(kursname))
		{
			this.kursname.set(kursname);
			notifyKurs();
		}
	}

	public Integer getID()
	{
		return ID.get();
	}

	public Kurs getKurs()
	{
		return kurs;
	}
}
