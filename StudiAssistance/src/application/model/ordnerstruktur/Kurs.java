package application.model.ordnerstruktur;

import java.util.StringTokenizer;

public class Kurs
{
	private final int ID;
	private String kursname;
	
	private KursAdapter kursAdapter;
	
	public Kurs(int ID)
	{
		this.ID = ID;
		this.kursname = "Default Kursname";
	}
	
	public Kurs(int ID, String Kursname)
	{
		this.ID = ID;
		this.kursname = Kursname;
	}

	public void notifyKursAdapter()
	{
		kursAdapter.setKursname(kursname);
	}
	
	public String getKursname()
	{
		return kursname;
	}

	public void setKursname(String kursname)
	{
		this.kursname = kursname;
	}

	public int getID()
	{
		return ID;
	}
	
	public void setKursAdapter(KursAdapter kursAdapter)
	{
		this.kursAdapter = kursAdapter;
	}
	
	public void shrinkString()
	{
		StringTokenizer st = new StringTokenizer(kursname);
		kursname = "";
		int anzToken = st.countTokens();
		for (int i=0; i<anzToken-1;i++)
		{
			kursname = kursname + " " + st.nextToken();
		}
		notifyKursAdapter();
	}
}
