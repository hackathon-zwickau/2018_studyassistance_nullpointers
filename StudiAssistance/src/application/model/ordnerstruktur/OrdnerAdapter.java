package application.model.ordnerstruktur;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrdnerAdapter
{
	private Ordner ordner;
	
	private StringProperty name;
	private StringProperty path;
	
	public OrdnerAdapter(Ordner ordner)
	{
		this.ordner = ordner;
		this.name = new SimpleStringProperty(ordner.getName());
		this.path = new SimpleStringProperty(ordner.getPath());
		ordner.setOrdnerAdapter(this);
	}
	
	public void notifyOrdner()
	{
		ordner.setName(name.get()); 
		ordner.setPath(path.get());
	}
	
	public void setName(String name)
	{
		if (!this.name.get().equals(name))
		{
			this.name.set(name);;
			notifyOrdner();
		}
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public void setPath(String path)
	{
		if (!this.path.get().equals(path))
		{
			this.path.set(path);;
			notifyOrdner();
		}
	}
	
	public String getPath()
	{
		return path.get();
	}
	
	public void setOrdner(Ordner ordner)
	{
		this.ordner = ordner;
	}
}
