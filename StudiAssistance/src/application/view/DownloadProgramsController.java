package application.view;

import java.util.ArrayList;

import application.Main;
import application.model.programmInstalation.DownloadViaURL;
import application.model.programmInstalation.NeededPrograms;
import application.model.programmInstalation.NeededProgramsAdapter;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DownloadProgramsController
{
	Main main;
	Stage stage;
	DownloadViaURL dvURL;
	NeededPrograms neededProgams;
	NeededProgramsAdapter programs;

	@FXML
	private TextField textField;
	@FXML
	private VBox vBox;
	@FXML
	private CheckBox checkBox1;
	@FXML
	private CheckBox checkBox2;
	@FXML
	private CheckBox checkBox3;
	@FXML
	private CheckBox checkBox4;
	@FXML
	private CheckBox checkBox5;

	private ArrayList<CheckBox> checkBoxList = new ArrayList<>();

	@FXML
	private Button download;
	@FXML
	private Button cancle;

	@FXML
	public void initialize ()
	{
		checkBox1.setVisible(false);
		checkBoxList.add(checkBox1);

		checkBox2.setVisible(false);
		checkBoxList.add(checkBox2);

		checkBox3.setVisible(false);
		checkBoxList.add(checkBox3);

		checkBox4.setVisible(false);
		checkBoxList.add(checkBox4);

		checkBox5.setVisible(false);
		checkBoxList.add(checkBox5);

	}

	@FXML
	public void handleCancleButton ()
	{
		stage.close();
	}

	@FXML
	public void handleDownloadButton ()
	{
		if (textField.getPromptText().equals(""))
		{
			// TODO ALERT!
			System.out.println(">>>>>>>>>>>>>>>>>> DOWNLOAD FAILED!");
		}
		else
		{

			for (CheckBox cBox : checkBoxList)
			{
				if (cBox.isSelected())
				{

					String cBoxName = cBox.getText();
					String urlLink = neededProgams.getDownloadLink(1, cBoxName);
					String downloadPath = textField.getText();

					dvURL = new DownloadViaURL(urlLink, downloadPath);
					dvURL.connectAndDownload(downloadPath, urlLink);
				}
			}

		}

	}

	public void setCheckers ()
	{
		int i = 0;
		ArrayList<String> iterList = new ArrayList<>();
		iterList.addAll(programs.getProgramNames());

		for (String name : iterList)
		{
			switch (i)
			{
			case (0) :
			{
				checkBox1.setVisible(true);
				checkBox1.setText(name);
			}
				break;
			case (1) :
			{
				checkBox2.setVisible(true);
				checkBox2.setText(name);
			}
				break;
			case (2) :
			{
				checkBox3.setVisible(true);
				checkBox3.setText(name);
			}
				break;
			case (3) :
			{
				checkBox4.setVisible(true);
				checkBox4.setText(name);
			}
				break;
			case (4) :
			{
				checkBox5.setVisible(true);
				checkBox5.setText(name);
			}
				break;
			}
			// iterator for the switch
			i++;
		}

	}

	public void setNeededProgramsAdapter (NeededProgramsAdapter programs)
	{
		this.programs = programs;
	}

	public void setNeededPrograms (NeededPrograms neededPrograms)
	{
		this.neededProgams = neededPrograms;
	}

	public void setMain (Main main)
	{
		this.main = main;
	}

	public final void setStage (Stage stage)
	{
		this.stage = stage;
	}

}
