package application.view;

import java.net.URI;
import java.net.URISyntaxException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import application.Main;
import application.model.ordnerstruktur.KursAdapter;
import application.model.ordnerstruktur.OrdnerAdapter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import utility.AdapterGenerator;
import utility.OrdnerConverter;

public class StudiAssistanceMainController
{

	private Stage stage;
	private Main main;
	private Boolean fileloaded = false;
	private Boolean dirloaded;

	private List<File> file	= new ArrayList<>();

	@FXML
	private TreeView<String> ordnerTree;

	@FXML
	private ListView<String> kursView;
	
	@FXML
	private ComboBox<OrdnerAdapter> ordnerBox;

	private ObservableList<KursAdapter> kursAdapter = FXCollections.observableArrayList();
	private ObservableList<OrdnerAdapter> ordnerAdapter = FXCollections.observableArrayList();

	private AdapterGenerator adapterGenerator = new AdapterGenerator();


	@FXML
	public void initialize ()
	{

	}

	public void handleInstallsem1 ()
	{

		main.initProgramsAdapter(1);
		main.setInstallView();
	}

	public void handleInstallsem2 ()
	{

		main.initProgramsAdapter(2);
		main.setInstallView();
	}

	public void handleInstallsem3 ()
	{

		main.initProgramsAdapter(3);
		main.setInstallView();
	}

	public void handleInstallsem4 ()
	{

		main.initProgramsAdapter(4);
		main.setInstallView();
	}

	public void handleOpalButton ()
	{
		try
		{
			java.awt.Desktop.getDesktop().browse(new URI("https://bildungsportal.sachsen.de/opal/shiblogin?0"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void handleRedminButton ()
	{
		try
		{
			java.awt.Desktop.getDesktop().browse(new URI(
					"https://projektserver.informatik.fh-zwickau.de/login?back_url=https%3A%2F%2Fprojektserver.informatik.fh-zwickau.de%2F"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void handleFragenToolButton ()
	{
		try
		{
			java.awt.Desktop.getDesktop().browse(new URI(
					"https://projektserver.informatik.fh-zwickau.de/login?back_url=https%3A%2F%2Fprojektserver.informatik.fh-zwickau.de%2F"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void handleMoodleButton ()
	{
		try
		{
			java.awt.Desktop.getDesktop().browse(new URI("http://moodle.wiwi.fh-zwickau.de/moodle/login/index.php"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void handlePraxisButton ()
	{
		try
		{
			java.awt.Desktop.getDesktop().browse(new URI("https://intranet.informatik.fh-zwickau.de/index.php?id=107"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void handlePlusLaenge ()
	{

	}

	public void handleMinusLaenge ()
	{
		if (fileloaded)
		{	
			String selection = kursView.getSelectionModel().getSelectedItem();
			for (KursAdapter k : kursAdapter)
			{
				if (k.getKursname().equals(selection))
				{
					k.getKurs().shrinkString();
				}
			}
		
			loadKursView();
		}
		else
		{
			fileNotLoaded();
		}
	}

	public void handleLinksVerschieben ()
	{
		
	}

	public void handleRechtsVerschieben ()
	{
		if (fileloaded)
		{
			String item = ordnerTree.getSelectionModel().getSelectedItem().getValue();
			String item2 = ordnerBox.getSelectionModel().getSelectedItem().getName();
			TreeItem<String> Item = null;
			TreeItem<String> Item2 = null;
			
			if (item!= null && item2 != null)
			{
				
				for(TreeItem<String> a: ordnerTree.getRoot().getChildren())
				{
					if (a.getValue().equals(item)) {
						
						Item = a;
					}
					if (a.getValue().equals(item2))
					{
						Item2 = a;
					}
				}
				
				if (Item != null && Item2 != null)
				{
					Item.getChildren().add(Item2);
					ordnerTree.getRoot().getChildren().add(Item);
				}
				remmoveOldItems(Item, Item2);
			}
			else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.initOwner(main.getStage());
				alert.setTitle("Auswahl fehlerhaft");
				alert.setHeaderText("Ihre Auswahl ist nicht vollst�ndig!");
				alert.setContentText("Bitte w�hlen Sie eine Ordner in der Baumstrukur und der Combobox aus.");
				alert.showAndWait();
			}
			
			
		}
		else
		{
			fileNotLoaded();
		}
	}

	public void handleOpenFile ()
	{

		Optional<String> absolutePath = main.fileChoser();
		if (absolutePath.isPresent())
		{
			String path = absolutePath.get();
			kursAdapter.clear();
			main.initializeKurs(path);		
			kursAdapter.addAll(adapterGenerator.kursAdapterGenerator(main.getKurs()));
			loadKursView();
			fileloaded = true;
		}
		else
		{
			fileloaded = false;
		}
		
	}

	public void handleErstelleOrdnerStrukur ()
	{
		if (fileloaded)
		{
			ordnerAdapter.clear();
			ordnerAdapter.addAll(adapterGenerator.ordnerAdapterGenerator(main.getKurs()));
			setTreeView();
			ordnerBox.setConverter(new OrdnerConverter());
			ordnerBox.setItems(ordnerAdapter);
		}
		else
		{
			fileNotLoaded();
		}
	}
	
	public void handleSelectLocation()
	{
		
		Optional<String> absolutePath = main.directoryChooser();
		if (absolutePath.isPresent())
		{
			String path = absolutePath.get();
			for (TreeItem<String> a:ordnerTree.getRoot().getChildren())
			{
				childrenPath(a, path);
				for(OrdnerAdapter b: ordnerAdapter)
				{
					if(b.getName().equals(a.getValue()))
					{
						b.setPath(path + "/" + b.getName());
					}
				}
			}
			dirloaded = true;
		}
		else
		{
			dirNotLoaded();
			dirloaded = false;
		}
	}
	
	public void handleErstellen()
	{
		if (dirloaded)
		{
			createDirectory();
		}
		else
		{
			dirNotLoaded();
		}
	}

	public void setMain (Main main)
	{
		this.main = main;
		
		

	}

	public void setStage (Stage stage)
	{
		this.stage = stage;
	}

	private void loadKursView ()
	{
		kursView.getItems().clear();
		ObservableList<String> kursAdapters = FXCollections.observableArrayList();
		for (KursAdapter k : kursAdapter)
		{
			kursAdapters.add(k.getKursname());
		}
		kursView.setItems(kursAdapters);
	}
	
	private void setTreeView()
	{
		TreeItem<String> rootItem = new TreeItem<String> ("");
		for (OrdnerAdapter oa:ordnerAdapter)
		{
			rootItem.getChildren().add(new TreeItem<String>(oa.getName()));
		}
        rootItem.setExpanded(true);
        ordnerTree.setRoot(rootItem);
	}
	
	public ObservableList<OrdnerAdapter>  getOrdnerTree()
	{
		return ordnerAdapter;
	}
	
	private void remmoveOldItems(TreeItem<String> item,TreeItem<String> item2)
	{
		ordnerTree.getRoot().getChildren().remove(item);
		ordnerTree.getRoot().getChildren().remove(item2);
	}
	
	private void childrenPath(TreeItem<String> item, String dir)
	{
		while(!item.getChildren().isEmpty())
			for(TreeItem<String> a:item.getChildren())
			{
				for(OrdnerAdapter b: ordnerAdapter)
				{
					if(b.getName().equals(a.getValue()))
					{
						b.setPath(dir + "/" + b.getName());
						System.out.println(b.getPath());
					}
				}
				childrenPath(item, dir);
			}
	}
	
	private void createDirectory()
	{
		try
		{
			for(OrdnerAdapter a: ordnerAdapter)
			{
				File fil = new File(a.getPath());
				this.file.add(fil);
				if (fil.mkdirs())
				{
					fil.createNewFile();
				}
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	private void fileNotLoaded()
	{
		Alert alert = new Alert(AlertType.WARNING);
		alert.initOwner(main.getStage());
		alert.setTitle("W�hle ein Stundenplan aus");
		alert.setHeaderText("Bitte w�hlen sie einen Stundenplan aus.");
		alert.setContentText("W�hlen Sie ein Stundenplan im format PDF aus.");
		alert.showAndWait();
	}
	
	private void dirNotLoaded()
	{
		Alert alert = new Alert(AlertType.WARNING);
		alert.initOwner(main.getStage());
		alert.setTitle("W�hle ein Directory aus");
		alert.setHeaderText("Bitte w�hlen sie ein Directory aus");
		alert.setContentText("W�hlen Sie einen Ordner, in dem Sie die neuen Ordner speichern wollen.");
		alert.showAndWait();
	}
}
